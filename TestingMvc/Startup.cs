﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestingMvc.Startup))]
namespace TestingMvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
